#! /usr/bin/python

# Setup script for the Pi Co-op

import shutil
import argparse
import inspect
import os.path
import sys
import subprocess

CMDLINE_FILENAME = "/boot/cmdline.txt"
CMDLINE_BACKUP_FILENAME = "/boot/cmdline.txt.dr_bak"

INITTAB_FILENAME = "/etc/inittab"
INITTAB_BACKUP_FILENAME = "/etc/inittab.dr_bak"

UDEV_RULES_FILENAME = "99-pi_co-op_tty.rules"

scriptPath = os.path.dirname( os.path.abspath ( inspect.getfile( inspect.currentframe() ) ) )
srcUdevRulesFilename = scriptPath + "/udev/" + UDEV_RULES_FILENAME
dstUdevRulesFilename =  "/etc/udev/rules.d/" + UDEV_RULES_FILENAME

autoresetFilename = scriptPath + "/avrdude/autoreset"
avrdudeAutoresetFilename = scriptPath + "/avrdude/avrdude-autoreset"

# Parse the command line
parser = argparse.ArgumentParser( description='Configures the Pi to work with a Pi Co-op' )
parser.add_argument( "command", choices=[ "install", "uninstall" ] )

args = parser.parse_args()

if args.command == "install":
        
    # Installation

    # Check that the Arduino environment is installed
    if not os.path.exists( "/usr/bin/arduino" ):
        
        print "Error: Unable to find the Arduino IDE."
        sys.exit( -1 )

    # Check that a backup file doesn't already exist
    if os.path.exists( CMDLINE_BACKUP_FILENAME ) or os.path.exists( INITTAB_BACKUP_FILENAME ):
        print "Error: Backup files already exist. A complete or partial installation has been done before"
        print "Please run 'sudo {0} uninstall' first".format( sys.argv[ 0 ] )
        sys.exit( -1 )

    # Back up existing serial port settings
    print "Backing up existing serial port settings..."
    shutil.copy( CMDLINE_FILENAME, CMDLINE_BACKUP_FILENAME )
    shutil.copy( INITTAB_FILENAME, INITTAB_BACKUP_FILENAME )

    # Modify the command line file
    print "Modifying {0}...".format( CMDLINE_FILENAME )

    origCmdLineFile = open( CMDLINE_BACKUP_FILENAME, "r" )
    newCmdLineFile = open( CMDLINE_FILENAME, "w" )

    origCmdLine = origCmdLineFile.read()
        
    newCmdLine = ""
    args = origCmdLine.split( " " )
    numArgsAdded = 0
    
    for arg in args:
        if arg == "console=ttyAMA0,115200" or arg == "kgdboc=ttyAMA0,115200":
            
            # These are the elements to be removed
            pass
        
        else:
            
            if numArgsAdded == 0:
                newCmdLine = arg
            else:
                newCmdLine = newCmdLine + " " + arg
                
            numArgsAdded += 1
                
    newCmdLineFile.write( newCmdLine )

    newCmdLineFile.close()
    origCmdLineFile.close()

    # Modify the inittab file
    print "Modifying {0}...".format( INITTAB_FILENAME )
    
    origInittabFile = open( INITTAB_BACKUP_FILENAME, "r" )
    newInittabFile = open( INITTAB_FILENAME, "w" )

    for line in origInittabFile:
        
        if line.startswith( "T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100" ):
            newInittabFile.write( "#" + line )
        else:
            newInittabFile.write( line )

    newInittabFile.close()
    origInittabFile.close()

    print "Creating ttyS0 udev rules..."

    # Create udev rules
    shutil.copy( srcUdevRulesFilename, dstUdevRulesFilename )

    # Modify avrdude
    print "Modifying avrdude..."
    if os.path.exists( "/usr/bin/avrdude-original" ):
        print "Error: Backup files for avrdude already exist. A complete or partial installation has been done before"
        print "Please run 'sudo {0} uninstall' first".format( sys.argv[ 0 ] )
        sys.exit( -1 )
        
    shutil.copy( autoresetFilename, "/usr/bin" )
    shutil.copy( avrdudeAutoresetFilename, "/usr/bin" )
    shutil.copy( "/usr/bin/avrdude", "/usr/bin/avrdude-original" )
    os.remove( "/usr/bin/avrdude" )
    subprocess.call( [ "ln", "-s", "/usr/bin/avrdude-autoreset", "/usr/bin/avrdude" ] )

elif args.command == "uninstall":

    # Uninstall process

    print "Removing ttyS0 udev rules..."

    # Remove udev rules
    if os.path.exists( dstUdevRulesFilename ):
        os.remove( dstUdevRulesFilename )
    
    # Restore serial port settings
    print "Restoring serial port settings..."
    
    if os.path.exists( CMDLINE_BACKUP_FILENAME ):
        shutil.copy( CMDLINE_BACKUP_FILENAME, CMDLINE_FILENAME )
        os.remove( CMDLINE_BACKUP_FILENAME )
    
    if os.path.exists( INITTAB_BACKUP_FILENAME ):
        shutil.copy( INITTAB_BACKUP_FILENAME, INITTAB_FILENAME )
        os.remove( INITTAB_BACKUP_FILENAME )
    
    # Restore avrdude
    print "Restoring avrdude..."
    
    if os.path.exists( "/usr/bin/avrdude-original" ):
        
        if os.path.exists( "/usr/bin/avrdude" ):
            os.remove( "/usr/bin/avrdude" )
            
        shutil.copy( "/usr/bin/avrdude-original", "/usr/bin/avrdude" )
        os.remove( "/usr/bin/avrdude-original" )
    