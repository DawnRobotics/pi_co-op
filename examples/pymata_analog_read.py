#! /usr/bin/python

# This script reads an analog input on pin 0, prints the result to the serial monitor.
# Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

import time

from PyMata.pymata import PyMata

# Create an instance of PyMata.
SERIAL_PORT = "/dev/ttyS0"
firmata = PyMata( SERIAL_PORT, max_wait_time=5 )

# Configure pin A0 as an analog input
firmata.set_pin_mode( 0, firmata.INPUT, firmata.ANALOG )

time.sleep( 0.2 ) # Allow some time for the first data to arrive from the Arduino and be processed

try:
    # run in a loop over and over again forever:
    while True:
        
        sensorValue = firmata.analog_read( 0 )    # read the input on analog pin 0
        print sensorValue                         # print out the value you read
  
except KeyboardInterrupt:

    # Catch exception raised by using Ctrl+C to quit
    pass

# close the interface down cleanly
firmata.close()

