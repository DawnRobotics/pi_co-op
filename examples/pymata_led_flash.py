#! /usr/bin/python

# This script flashes a series of LEDs, one after another

import time

from PyMata.pymata import PyMata

LED_PINS = [ 5, 10, 12 ]
LED_TIME = 0.2

# Create an instance of PyMata.
SERIAL_PORT = "/dev/ttyS0"
firmata = PyMata( SERIAL_PORT, max_wait_time=5 )

# Set up the LED pins
for pin in LED_PINS:
    firmata.set_pin_mode( pin, firmata.OUTPUT, firmata.DIGITAL )
    firmata.digital_write( pin, firmata.LOW )

try:
    while True:
        
        for pin in LED_PINS:

            # Turn on the selected LED
            firmata.digital_write( pin, firmata.HIGH )

            # Turn off the other LEDs
            for otherPin in LED_PINS:

                if otherPin != pin:
                    firmata.digital_write( otherPin, firmata.LOW )

            time.sleep( LED_TIME )

except KeyboardInterrupt:

    # Catch exception raised by using Ctrl+C to quit
    pass

# close the interface down cleanly
firmata.close()


