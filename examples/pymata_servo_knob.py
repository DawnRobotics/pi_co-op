#! /usr/bin/python

# Controlling a servo position using a potentiometer (variable resistor) using PyMata

import time

from PyMata.pymata import PyMata

# Choose the pin used to control the servo, and the analog pin to read from
SERVO_PIN = 9
POTENTIOMETER_PIN = 0

# Create an instance of PyMata.
SERIAL_PORT = "/dev/ttyS0"
firmata = PyMata( SERIAL_PORT, max_wait_time=5 )

# Set up the servo pin
firmata.servo_config( SERVO_PIN )

# Configure potentiometer pin as an analog input
firmata.set_pin_mode( POTENTIOMETER_PIN, firmata.INPUT, firmata.ANALOG )

time.sleep( 0.2 ) # Allow some time for the first data to arrive from the Arduino and be processed

try:
    # run in a loop over and over again forever:
    while True:
        
        val = firmata.analog_read( POTENTIOMETER_PIN )          # reads the value of the potentiometer (value between 0 and 1023) 
        
        # scale it to use it with the servo (value between 0 and 180)
        val = int( 180.0*float( val )/1023.0 )
        val = max( 0, min( val, 180 ) )
        
        firmata.analog_write( SERVO_PIN, val )     # sets the servo position according to the scaled value 
        time.sleep( 0.015 )                        # waits for the servo to get there 
  
except KeyboardInterrupt:

    # Catch exception raised by using Ctrl+C to quit
    pass

# close the interface down cleanly
firmata.close()

