#! /usr/bin/python

# This script demonstrates how to recreate the basic blink sketch using PyMata

import time

from PyMata.pymata import PyMata

# Pin 13 has an LED connected on most Arduino boards.
# give it a name:
LED = 13

# Create an instance of PyMata.
SERIAL_PORT = "/dev/ttyS0"
firmata = PyMata( SERIAL_PORT, max_wait_time=5 )

# initialize the digital pin as an output.
firmata.set_pin_mode( LED, firmata.OUTPUT, firmata.DIGITAL )

try:
    # run in a loop over and over again forever:
    while True:
        
        firmata.digital_write( LED, firmata.HIGH )   # turn the LED on (HIGH is the voltage level)
        time.sleep( 1.0 )                            # wait for a second
        firmata.digital_write( LED, firmata.LOW )    # turn the LED off by making the voltage LOW
        time.sleep( 1.0 )                            # wait for a second

except KeyboardInterrupt:

    # Catch exception raised by using Ctrl+C to quit
    pass

# close the interface down cleanly
firmata.close()


