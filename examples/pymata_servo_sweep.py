#! /usr/bin/python

# This script demonstrates how to recreate the servo sweep sketch using PyMata

import time

from PyMata.pymata import PyMata

# Choose the pin used to control the servo
SERVO_PIN = 9

# Create an instance of PyMata.
SERIAL_PORT = "/dev/ttyS0"
firmata = PyMata( SERIAL_PORT, max_wait_time=5 )

# Set up the servo pin
firmata.servo_config( SERVO_PIN )

try:
    # run in a loop over and over again forever:
    while True:
        
        for pos in range( 180 ):                       # goes from 0 degrees to 180 degrees in steps of 1 degree 
            firmata.analog_write( SERVO_PIN, pos )     # tell servo to go to position in variable 'pos' 
            time.sleep( 0.015 )                        # waits 15ms for the servo to reach the position 

        for pos in range( 180, 0, -1 ):                # goes from 180 degrees to 0 degrees 
            firmata.analog_write( SERVO_PIN, pos )     # tell servo to go to position in variable 'pos' 
            time.sleep( 0.015 )                        # waits 15ms for the servo to reach the position 
  
except KeyboardInterrupt:

    # Catch exception raised by using Ctrl+C to quit
    pass

# close the interface down cleanly
firmata.close()

