Software for the Dawn Robotics Pi Co-op board - a Raspberry Pi Arduino add-on board.

This repository contains a setup script to help configure the Raspberry Pi for use with the Pi Co-op.

You can find out more about the Pi Co-op, along with a manual and schematic at [http://www.dawnrobotics.co.uk/pi-co-op-raspberry-pi-arduino-add-on-board/](http://www.dawnrobotics.co.uk/pi-co-op-raspberry-pi-arduino-add-on-board/)

To start using the Pi Co-op on a graphical Raspbian installation

Installation
------------

Install the Arduino IDE

    sudo apt-get update
	sudo apt-get install arduino git

This will install the Arduino IDE and some supporting Python libraries. Now run the following commands

	git clone https://bitbucket.org/DawnRobotics/pi_co-op.git
	cd pi_co-op
	sudo python setup_pi_co-op.py install

This will alter configuration files to allow us to use the serial port on the GPIO pins of the Pi. Finish the configuration by restarting the Raspberry Pi

	sudo reboot

You can now open up the Arduino IDE and program the Pi Co-op. The board type is Arduino Uno and the serial port is ttyS0.

Using the Pi Co-op with PyMata
------------------------------

The examples folder contains some python scripts which show how to control the Pi Co-op with [PyMata](https://github.com/DawnRobotics/PyMata), a Python client library written by [Alan Yorinks](https://sites.google.com/site/mryslabnotebook/) for [Firmata](http://firmata.org/wiki/Main_Page).

PyMata makes use of a library called [Ino](http://inotool.org/). To install PyMata and Ino on your Raspberry Pi run

    sudo apt-get update
    sudo apt-get install python-dev python-pip python-serial
    sudo pip install ino
    
    cd ~
    git clone https://github.com/DawnRobotics/PyMata.git
    cd PyMata
    sudo python setup.py install
    
Now you can make the LED on the Pi Co-op blink by running

    cd pi_co-op
    ./examples/pymata_blink.py
    
**Please Note:** If you do not have Firmata installed on your Pi Co-op then the first time you use PyMata there will be quite a delay as PyMata is compiled and uploaded to the Pi Co-op. Things are much quicker if Firmata is already built or already on the Pi Co-op.
    
    

Have fun!
